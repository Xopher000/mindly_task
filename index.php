<?php
//this is our main page, which displays transactions already entered into the table and allows the user to enter new transactions or delete old ones
echo "<html><head>";
echo "<title> Mindly Web App </title>";
echo "<link rel=\"shortcut icon\" type =\"image/png\" href=\"./img/mindly-favicon.png\" />";
echo "<link rel=\"stylesheet\" href=\"./generalStyle.css?ts=2018999\" />";
echo "</head><body>";

        //connect to database
        require("./conn.php");

        //Here we want to display all the rows in our main table
        //we create a query that retrives all of the rows from cryptoPortfolio
        $trans = "select * from cryptoPortfolio";
        $t = mysql_query($trans);

                        //we create a table to display our data 
                        echo "<table class = \"darkTable\">";
                        echo "<th> Cryptocurrency </th><th> Amount </th><th> Date of purchase </th> <th> Wallet location </th><th> Current markert value (EUR) </th> <th> Option </th>";
                                        
                                        //we loop through the table and print out every row
                                        while($row = mysql_fetch_array($t))
                                        {
                                                echo "<tr>";
                                                echo "<td> {$row['currency']} </td>";
                                                echo "<td> {$row['amount']} </td>";
                                                $dateFormatted = date("d.m.Y", strtotime($row['dateOfPurchase'])); 
                                                echo "<td>".$dateFormatted."</td>";   //print date in day . month . year format
                                                echo "<td> {$row['walletLocation']} </td>";
                                                echo "<td> {$row['marketValue']} </td>";
                        
                                                //every row has a button that can be used to delete it
                                                $vars = array( 'id' => $row['id']);
                                                
                                                //every row has a randomly created primary key 'id'; this is url-encoded before being passed to our delete method via GET 
                                                $querystring = http_build_query($vars);
                                                
                                                //when the user clicks 'delete', they trigger a script asks for confirmation
                                                echo "<td><a href=\"./delete.php?".$querystring."\" onclick=\"return confirm('Do you want to delete? Y/N')\">Delete</a></td>";
                                        }

                                        //print form for inserting a new row
                                        echo "<form action=\"./insert.php\" method=\"post\"><div id=\"content\"><tr>";
                                        
                                        //create a drop down menu for selecting cryptocurrency
                                        echo "<td><select name =\"currency\">";
                                        echo "<option value=\"Bitcoin\" selected>Bitcoin</option>";
                                        echo "<option value=\"Ethereum\">Ethereum</option>";
                                        echo "<option value=\"Ripple\">Ripple</option>";
                                        echo "<option value=\"Litecoin\">Litecoin</option>";
                                        echo "<option value=\"Dash\">Dash</option>";
                                        echo "<option value=\"Zcash\">Zcash</option></select></td>";
                                        
                                        //input field for amount of cryptocurrency purchased
                                        echo "<td><input type=\"text\" style=\"width: 65px;\" name=\"amount\"></td>";
                                        
                                        //input field for date crypto currency purchased - in year - month - day format
                                        $today = date("Y-m-d");        //set default value in field as today's date
                                        echo "<td><input type=\"date\" value={$today} name=\"dateOfPurchase\">";
                                        
                                        //input field for location of wallet that cryptocurrency is stored in
                                        echo "<td><input type=\"text\" name=\"walletLocation\"></td>";
                                        
                                        //user cannot input market value in euros - determined by function called in insert.php
                                        echo "<td><input type=\"number\"  style=\"width: 65px;\" name=\"marketValue\" disabled ></td>";
                                        
                                        //button for inserting new row - passes user input as POST data
                                        echo "<td><input type=\"submit\" value=\"Save\"></td></tr></div></form></table>";

                echo "</body><footer id=\"footer\" style=\"color:white\"><img src=\"./img/mindly-logo-purple-tree.png\" height=\"20px\"></footer></html>";
                
                ?>