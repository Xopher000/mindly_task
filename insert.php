<?php
        require("./conn.php");
        
        //here we call out coinapi, which has many functions for tracking and trading cryptocurrency
        require_once('./coinapi.inc.php');
        
        //here we use our api key to create a new instance of the CoinAPI class
        $api_key = "E6DDB775-46AC-41B6-80C2-6D9D77CDD2D7";
        $capi = new CoinAPI($api_key);

                //input is passed in through POST, assigned to variables we use to insert a new row
                
                $dirtyCurrency = $_POST['currency'];
                $currency = preg_replace('/[^a-zA-Z]/', '', $dirtyCurrency);
                //sanitize input passed to $currency
                
                $dirtyAmount = (float)$_POST['amount'];
                $amount = preg_replace('/[^0-9.]+$/i', '', $dirtyAmount);
                //sanitize input passed to $amount
                
                $dateOfPurchase = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['dateOfPurchase'])));
                
                $dirtyWalletLocation = $_POST['walletLocation'];
                $walletLocation = preg_replace('/[^A-Za-z0-9 ]/', '', $dirtyWalletLocation);
                //sanitize $walletLocation
                
                $marketValue = $amount;
                //marketValue is initially set equal to amount - this will be multiplied by the exchange rate

                //here, we determine what exchangerate we want to look at depending on the value of $currency
                   if     ($currency == 'Bitcoin') { $asset_id_base = 'BTC'; }
                   elseif ($currency == 'Ethereum'){ $asset_id_base = 'ETH'; }
                   elseif ($currency == 'Litecoin'){ $asset_id_base = 'LTC'; }
                   elseif ($currency == 'Ripple')  { $asset_id_base = 'XRP'; }
                   elseif ($currency == 'Zcash')   { $asset_id_base = 'ZEC'; }
                   elseif ($currency == 'Dash')    { $asset_id_base = 'DASH';}

                //here we format the date before passing it to the exchange rate function
                $today = date_create(date("Y-m-d"));
                
                //we use the GetExchangeRate function from the coinapi to get the exchange rate to euros
                //the parameters we pass in tell the function to give us the current exchange rate for our $currency to Euros
                
                //this if statement lets us test our insert function without requesting info from the api
                if($amount != 0){
                
                     $data = $capi->GetExchangeRate($asset_id_base, 'EUR', $today);
                     //here we get the marketValue for when our currency was bought
                     //$data is set equal to the function's json output, which has already been decoded.
                
                     //here we parse that output to find our rate
                     $rate = $data['rate'];
                
                     if(!$rate){   
                
                          /*if our function fails to parse the exchange rate from the json data, we likely have used up our daily limit of api requests
                           A pop-up window shows the message saying as much, passed in json by the api: " Too many requests - You have exceeded your API key daily rate limits,
                           wait until midnight UTC for reset, upgrade your plan or contact support about enabling automatic overcharge" - we are then redirected back to the main page*/
                          
                          echo "<script type=\"text/javascript\">", "alert(\" ".$data." \");", "</script>"; 
                          header("Location: http://18.195.160.243/mindly_task/index.php");
                      }
                
                     //we set the marketvalue equal to the rate * the amount
                     //round the rate to 3 decimal places
                     $marketValue *= round($rate, 3);
                }
                
                //now we assign a random value to the row we insert, which will be used as a primary key.
                //this will be used if we want to delete that row later
                $id = rand();

                   //here we create our insertion query
                   $insert = "INSERT INTO cryptoPortfolio (currency, amount, dateOfPurchase, walletLocation, marketValue, id)";
                   $insert .= "VALUES ('$currency', '$amount', '$dateOfPurchase', '$walletLocation', '$marketValue', '$id')";
                   
                   //Now execute the insertion query
                   if(!mysql_query($insert)) {
                        echo "<p>Not Inserted</p>";
                   } else {
                        //otherwise the user is given a notification that the query failed
                        echo "<p>Inserted</p>";
                   }
                
                //redirect back to main page
                header("Location: http://18.195.160.243/mindly_task/index.php");
?>
